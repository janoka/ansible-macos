# Macbook Ansible Install

Thanks for [Jeff Geerling](https://www.jeffgeerling.com/), inspired by his [repository](https://github.com/geerlingguy/mac-dev-playbook).

## Prerequisite

1. Ensure Apple's command line tools are installed (xcode-select --install to launch the installer).
2. [Install Brew](https://brew.sh/#install)
3. Update Brew and Install Ansible:

   ```shell
   brew update
   brew install ansible
   ```

4. Clone this repository to your local drive.
5. Run requirements inside the cloned directory:

    ```shell
    ansible-galaxy collection install --ignore-certs -r requirements.yml
    ```

6. Enter your account password when prompted.

    ```shell
    ansible-playbook macos.yml --ask-become-pass
    ```
